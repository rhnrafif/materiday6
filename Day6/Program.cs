﻿//class Program
//{
//    static void Main()
//    {
//        //object adalah implementasi dari class. class adalah blueprint dari sebuah object
//        // Animal animal = new Animal(); //instance default constructor
//        Animal animal = new Animal(4,2);

//        animal.Melihat();

//        Animal bebek = new Animal(animal);
//        animal.Melihat();

//    }
//}



//dalam class ada 5 kontruktor
//default, parameterize, copy, private, static
//instance adalah object

//class Animal
//{
//    public int Mata { get; set; }
//    public int Kaki { get; set; }


//    public Animal() //default contructor -- tanpa parameterize
//    {
//        Console.WriteLine("Animal");
//    }

//    public Animal(int mata, int kaki)
//    {
//        this.Mata = mata;
//        this.Kaki = kaki;
//    }
//    public void Melihat()
//    {
//        Console.WriteLine($"Animal melihat menggunakan {Mata} mata");
//    }

//    public Animal (Animal animal)  //copy constyctor.. mengirimkan class itu sendiri sebagai paramater
//    {
//        this.Kaki = animal.Kaki;
//        this.Mata = animal.Mata;
//    }

//}

//private const : constr yg dibuat dengan private modifier. tidak memungkin class lain mengvreate instance

//class Program
//{
//    static void Main()
//    {




//    }
//    class Animal //class adalah blueprint dari sebuah objcet
//    {
//        public static int Ekor { get; set; }

//        static Animal()
//        {
//            Console.WriteLine("static constructor");
//        }

//        public void Makan()
//        {
//            Console.WriteLine("Animal itu makan dengan satu ekor");
//        }

//    }

//}

//static constructor
//class Program
//{
//    static void Main()
//    {
//        Animal animal = new Animal(2); //deklrasi static constructor// static hany akan dipanggil sekali

//        Animal kucing = new Animal(4);



//    }
//    class Animal //class adalah blueprint dari sebuah objcet
//    {
//        public static int Ekor { get; set; }

//        static Animal()
//        {
//            Console.WriteLine("static constructor");
//        }

//        public Animal(int ekor)
//        {
//            Console.WriteLine("Instance Const");

//        }

//        //destructor adlh method untuk menghancurkan object

//        ~Animal() //desctructor akan melakukan pengosongan memori stelah object dibuat
//        {
//            Console.WriteLine("Destructor");
//        }
//    }

//}


//OOP
//1. Polimorphis : object yang memiliki banyak bentuk, Contoh Animal:Kucing, Bebek
//Impelementasi Polimhorphism : 1. Method Overloading, 2. method overRiding, Method Hiding.

// 1.1 Method OverLoading 
//Salah satu cara implementasi dari polimorphis, yaitu dengan membuat 2 atau lebih method yang sama
// 1.1 Method OverLoading 
//Salah satu cara implementasi dari polimorphis, yaitu dengan membuat 2 atau lebih method yang sama
//class Program
//{
//    public int Add(int a, int b) {
//        int hasil = a + b;
//        return hasil;
//    }

//    public int Add(int a, int b, int c)
//    {
//        int hasil = a + b + b;
//        return hasil;
//    }

//    public string NamaLengkap(string depan, string belakang)
//    {
//        var fullName = depan + " " + belakang;
//        return fullName;
//    }
//    static void Main() //entry point
//    {
//        Program program = new Program();

//        var hasil1 = program.Add(1, 4); //ini method 1
//        Console.WriteLine(hasil1);

//        var hasil2 = program.Add(2, 5, 6); //ini method 2
//        Console.WriteLine(hasil2);

//        var namaLengkap = program.NamaLengkap("Raihanudin", "Rafif");
//        Console.WriteLine(namaLengkap);
//    }
//}

// 1.2 Method OverRiding ;
// 1.3 Method Hiding

//class Program
//{

//    static void Main() //entry point
//    {
//        Animal animal = new Kucing();
//        Animal animalBebek = new Bebek(); //class bebek tidak melakukan override, oleh krn itu hanya memanggil method dari parent class

//        animal.MakeSound(); //output dari override
//        animalBebek.MakeSound(); //output dari parent class

//        Burung burung = new Burung(); //method hiding
//        burung.Cicuit();
//    }
//}
//class Animal //parent, base
//{
//    public virtual void MakeSound() //keyword virtual utk memungkinkan oeverride
//    {
//        Console.WriteLine("Suara Hewan");
//    }
//}

//class Kucing : Animal //child, derived
//{
//    public override void MakeSound() //wajib diberikan key override utk mengisi ulang method dr parent
//    {
//        Console.WriteLine("Suara Kucing");
//    }
//}

//class Bebek : Animal //child
//{
//    public void MakeSound()  //tidak mengisi ulang method parents
//    {

//    }
//}

//class Burung : Animal
//{
//    public void Cicuit()
//    {
//        Console.WriteLine("Suara Burung");
//    }
//}




//2. encapsulation
//3. Inheritance //c# tidak mendukung multi inheritance.

//4. Abstraction
// mengidentifikasi hanya beberapa karakteristik dari sebuah object. Abstraction tidak bisa di create instance, harus dikombinasikan dengan pholimorphism
//semua class dlm abstarct harus di overide ketika inheritance
//class Program
//{
//    static void Main()
//    {

//        //Shape shape = new Shape(); //ini akan error, krn abstract tidak bisa di crarte instance
//        Square square = new Square(8); //ini valid
//        int res = square.area();
//        Console.WriteLine($"Hasil dari perkalian kubus dengan besar sisi 8 adalah {res}");

//        square.CetakConsole();

//    }

//    abstract class Shape
//    {
//        public abstract int area();
//        public abstract int Luas();

//        public void CetakConsole()
//        {
//            Console.WriteLine("dapat membuat local method di abstarct");
//        }
//    }

//    class Square : Shape
//    {
//        private int side;

//        public Square(int x )
//        {
//            side = x;
//        }

//        public override int area()
//        {
//            Console.WriteLine("area of square");
//            return side * side;

//        }
//        public override int Luas()
//        {
//            return 1;
//        }
//    }

//    class Rectangle : Shape //ini juga harus diimplementasikan abstarctnya
//    {
//        public override int area()
//        {
//            throw new NotImplementedException();
//        }

//        public override int Luas()
//        {
//            throw new NotImplementedException();
//        }
//    }
//}

//GETTER SETTER

class Program
{
    static void Main()
    {
        Student student = new Student("rafif");
        student.Id = 2;

        Console.WriteLine(student.Id);
        Console.WriteLine(student.Nama);

        student.Alamat = "Tangerang";
        Console.WriteLine(student.Alamat);
    }
}
class Student
{
    //getter setter cara 1
    public int Id { get; set; } //ini sebenarnya private

    private int _age;

    //cara 2
    //public void GetAge(int age)
    //{
    //    _age = age;
    //}

    //public int GetAge()
    //{
    //    return _age;
    //}


    public string Nama { get; private set; }

    public Student (string nama)
    {
        Nama = nama;
    }


    //getter setter cara 3
    private string _alamat;

    public string Alamat
    {
        get
        {
            return _alamat;
        }
        set
        {
            _alamat = value;
        }
    }
}